require 'sinatra'
require 'redis'

# Get Redis configuration from environment variables
redis_host = ENV['REDIS_HOST'] || 'localhost'
redis_port = ENV['REDIS_PORT'] || '6379'
redis_pass = ENV['REDIS_PASS']
redis_db = ENV['REDIS_DB'] || '0'

# Connect to Redis
redis = Redis.new(
  host: redis_host,
  port: redis_port,
  password: redis_pass,
  db: redis_db
)

# Define routes
get '/' do
  'Hello, this is a simple web server using Redis!'
end

get '/set/:key/:value' do
  key = params['key']
  value = params['value']
  redis.set(key, value)
  "Set #{key} to #{value}"
end

get '/get/:key' do
  key = params['key']
  value = redis.get(key)
  "Value of #{key}: #{value}"
end

# Run the server
set :bind, '0.0.0.0'
set :port, 8888
