### README.md

The application deployment process has two stages:
- Build
- Deployment

To build an application, run this command:
```
docker build --push -f Dockerfile -t $IMAGE_TAG .
```

The next step is to create the `.values.yaml` file. This file contains the data required for Helm Chart. 
It also contains the sensitive variables like `REDIS_HOST` and `REDIS_PASS`, which can be passed from the CI environment using a protected repo variables, the `.env` file, or Hashicorp Vault secret storage.

Since I haven't set up a CI/CD system, I use a simple command that passes my ENVs from the `.env` file to the `consul-template` command to create a `.values.yaml`:
```
env $(cat .env | xargs) consul-template -template .helm_values.yaml:.values.yaml -once
```

After the file is generated, it remains to deploy the application using helm:
```
helm upgrade --install ruby-web ./chart \
  --namespace itasks \
  --values .values.yaml \
  --cleanup-on-fail --atomic \
  --history-max 3
```
