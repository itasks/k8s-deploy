FROM ruby:3-alpine

ENV USER ruby
ENV GROUP ruby

# Create a non-root user
RUN addgroup $GROUP && \
    adduser \
    --disabled-password \
    --no-create-home \
    --ingroup "$GROUP" \
    $USER

WORKDIR /home/ruby
COPY --chown=ruby:ruby . .
# Set only execute permission to server.rb
RUN chown root:root server.rb && \
    chmod 755 server.rb

# Run the following commands as $USER
USER $USER
RUN bundle install
EXPOSE 8888
ENTRYPOINT ["ruby", "server.rb"]
